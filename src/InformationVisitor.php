<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use ArrayIterator;
use Iterator;

/**
 * InformationVisitor class file.
 * 
 * This class is a no-op implementation of the InformationVisitorInterface.
 * 
 * @author Anastaszor
 * @template T
 * @implements \PhpExtended\Information\InformationVisitorInterface<T>
 */
class InformationVisitor implements InformationVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitAll()
	 */
	public function visitAll(array $informationIterator)
	{
		return $this->visitIterator(new ArrayIterator($informationIterator));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitIterator()
	 * @param Iterator<InformationInterface> $informationIterator
	 */
	public function visitIterator(Iterator $informationIterator)
	{
		$lres = null;
		
		foreach($informationIterator as $information)
		{
			$lres = $this->visitInformation($information);
		}
		
		return $lres;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitInformation()
	 */
	public function visitInformation(InformationInterface $information)
	{
		return $information->beVisitedBy($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 * @return ?T
	 */
	public function visitTriple(InformationTripleInterface $information)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitObject()
	 * @return ?T
	 */
	public function visitObject(InformationObjectInterface $information)
	{
		return null;
	}
	
}
