<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use Psr\SimpleCache\CacheInterface;

/**
 * SimpleCacheInformationVisitor class file.
 * 
 * This class is a visitor that uses cache to get data to assume that some
 * informations were already visited. It uses the etags of information to
 * know if an information is worth to pass thru.
 * 
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
class SimpleCacheInformationVisitor extends InformationVisitor implements InformationVisitorInterface
{
	
	/**
	 * The cache this object will use.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_cache;
	
	/**
	 * The visitor to which all the informations will be forwarded.
	 * 
	 * @var InformationVisitorInterface<boolean>
	 */
	protected InformationVisitorInterface $_visitor;
	
	/**
	 * Builds a new SimpleCacheInformationVisitor with the given cache and 
	 * inner visitor.
	 * 
	 * @param CacheInterface $cache
	 * @param InformationVisitorInterface<boolean> $visitor
	 */
	public function __construct(CacheInterface $cache, InformationVisitorInterface $visitor)
	{
		$this->_cache = $cache;
		$this->_visitor = $visitor;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitor::visitTriple()
	 * @return null|boolean
	 */
	public function visitTriple(InformationTripleInterface $information) : ?bool
	{
		if($information->isCacheable())
		{
			$key = \sha1($information->getSubject().'|'.$information->getPredicate());
			
			try
			{
				$cachedValue = $this->_cache->get($key);
				$etag = $information->getEtag();
				if($cachedValue === $etag)
				{
					return true;
				}
				$this->_cache->set($key, $etag);
			}
			// @codeCoverageIgnoreStart
			catch(\Psr\SimpleCache\InvalidArgumentException $exc)
			{
				// ignore, should not happen (keys are hashed)
			}
			// @codeCoverageIgnoreEnd
		}
		
		return $this->_visitor->visitTriple($information);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitor::visitObject()
	 * @return null|boolean
	 */
	public function visitObject(InformationObjectInterface $information) : ?bool
	{
		if($information->isCacheable())
		{
			$pkeys = $information->getPrimaryKey();
			\ksort($pkeys);
			$keys = [];
			
			foreach($pkeys as $key => $value)
			{
				$keys[] = $key.'='.$value;
			}
			$key = \sha1(\implode('|', $keys));
			
			try
			{
				$cachedValue = $this->_cache->get($key);
				$etag = $information->getEtag();
				if($cachedValue === $etag)
				{
					return true;
				}
				$this->_cache->set($key, $etag);
			}
			// @codeCoverageIgnoreStart
			catch(\Psr\SimpleCache\InvalidArgumentException $exc)
			{
				// ignore, should not happen (keys are hashed)
			}
			// @codeCoverageIgnoreEnd
		}
		
		return $this->_visitor->visitObject($information);
	}
	
}
