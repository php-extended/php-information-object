<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use DateTimeImmutable;
use Exception;
use Stringable;
use SummableDateInterval;

/**
 * InformationProfile class file.
 * 
 * This class represents an aggregated information about process times for 
 * informations.
 * 
 * @author Anastaszor
 */
class InformationProfile implements Stringable
{
	
	/**
	 * The information category for this profile. 
	 * 
	 * @var string
	 */
	protected string $_informationCategory;
	
	/**
	 * The number of informations that were processed.
	 * 
	 * @var integer
	 */
	protected int $_nbInformations = 0;
	
	/**
	 * The total time that those informations have been processed (cumulated).
	 * 
	 * @var SummableDateInterval
	 * @psalm-suppress PropertyNotSetInConstructor
	 */
	protected SummableDateInterval $_totalTime;
	
	/**
	 * Builds a new InformationProfile based on the given category.
	 * 
	 * @param string $informationCategory
	 */
	public function __construct(string $informationCategory)
	{
		$this->_informationCategory = $informationCategory;
		$this->_nbInformations = 0;
		
		try
		{
			$this->_totalTime = new SummableDateInterval('PT0S');	// + 0 sec
		}
		// @codeCoverageIgnoreStart
		/** @phpstan-ignore-next-line */ // does not recognize the throw
		catch(Exception $exc)
		{
			// should not happen
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->_informationCategory;
	}
	
	/**
	 * Handles the given information and extracts its computation duration.
	 * 
	 * @param InformationInterface $information
	 */
	public function handle(InformationInterface $information) : void
	{
		$this->_nbInformations++;
		$now = new DateTimeImmutable();
		$this->_totalTime->add($now->diff($information->getCreationDate()));
	}
	
	/**
	 * Gets the information category this profile is on.
	 * 
	 * @return string
	 */
	public function getInformationCategory() : string
	{
		return $this->_informationCategory;
	}
	
	/**
	 * Gets the number of informations processed.
	 * 
	 * @return integer
	 */
	public function getNbInformations() : int
	{
		return $this->_nbInformations;
	}
	
	/**
	 * Gets the total time those informations have been processed (cumulative).
	 * 
	 * @return SummableDateInterval
	 */
	public function getTotalTime() : SummableDateInterval
	{
		return $this->_totalTime;
	}
	
}
