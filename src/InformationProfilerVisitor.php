<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use Iterator;

/**
 * InformationProfilerVisitor class file.
 * 
 * This class profiles the data manipulation of those informations.
 * 
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
class InformationProfilerVisitor extends InformationVisitor implements InformationVisitorInterface
{
	
	/**
	 * The profiles about triples.
	 * 
	 * @var array<string, InformationProfile>
	 */
	protected array $_triples = [];
	
	/**
	 * The profiles about object informations.
	 * 
	 * @var array<string, InformationProfile>
	 */
	protected array $_objects = [];
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitIterator()
	 * @param Iterator<InformationInterface> $informationIterator
	 * @return null|boolean
	 */
	public function visitIterator(Iterator $informationIterator) : ?bool
	{
		$lres = true;
		
		foreach($informationIterator as $information)
		{
			$lres = $this->visitInformation($information) && $lres;
		}
		
		return $lres;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 * @return null|boolean
	 */
	public function visitTriple(InformationTripleInterface $information) : ?bool
	{
		if(!isset($this->_triples[$information->getSupportClass()]))
		{
			$this->_triples[$information->getSupportClass()] = new InformationProfile($information->getSupportClass());
		}
		
		$this->_triples[$information->getSupportClass()]->handle($information);
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitObject()
	 * @return null|boolean
	 */
	public function visitObject(InformationObjectInterface $information) : ?bool
	{
		if(!isset($this->_objects[$information->getSupportClass()]))
		{
			$this->_objects[$information->getSupportClass()] = new InformationProfile($information->getSupportClass());
		}
		
		$this->_objects[$information->getSupportClass()]->handle($information);
		
		return true;
	}
	
	/**
	 * Gets the profiles on triple informations.
	 * 
	 * @return array<integer, InformationProfile>
	 */
	public function getTripleProfiles() : array
	{
		return \array_values($this->_triples);
	}
	
	/**
	 * Gets the profiles on object informations.
	 * 
	 * @return array<integer, InformationProfile>
	 */
	public function getObjectProfiles() : array
	{
		return \array_values($this->_objects);
	}
	
}
