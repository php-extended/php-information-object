<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use Psr\Cache\CacheItemPoolInterface;

/**
 * CachePoolInformationVisitor class file.
 * 
 * This class is a visitor that uses cache to get data to assume that some
 * informations were already visited. It uses the etags of information to
 * know if an information is worth pass thru.
 * 
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
class CacheItemPoolInformationVisitor extends InformationVisitor implements InformationVisitorInterface
{
	
	/**
	 * The cache this object will use.
	 * 
	 * @var CacheItemPoolInterface
	 */
	protected CacheItemPoolInterface $_cache;
	
	/**
	 * The visitor to which all the informations will be forwarded.
	 * 
	 * @var InformationVisitorInterface<boolean>
	 */
	protected InformationVisitorInterface $_visitor;
	
	/**
	 * Builds a new CacheItemPoolInformationVisitor with the given cache and
	 * inner visitor.
	 * 
	 * @param CacheItemPoolInterface $cache
	 * @param InformationVisitorInterface<boolean> $visitor
	 */
	public function __construct(CacheItemPoolInterface $cache, InformationVisitorInterface $visitor)
	{
		$this->_cache = $cache;
		$this->_visitor = $visitor;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitor::visitTriple()
	 * @return null|boolean
	 */
	public function visitTriple(InformationTripleInterface $information) : ?bool
	{
		if($information->isCacheable())
		{
			$key = \sha1($information->getSubject().'|'.$information->getPredicate());
			
			try
			{
				$cacheItem = $this->_cache->getItem($key);
				$etag = $information->getEtag();
				if($cacheItem->isHit() && $cacheItem->get() === $etag)
				{
					return true;
				}
				$cacheItem->set($etag);
				$this->_cache->save($cacheItem);
			}
			// @codeCoverageIgnoreStart
			catch(\Psr\Cache\InvalidArgumentException $exc)
			{
				// ignore, should not happen (keys are hashed)
			}
			// @codeCoverageIgnoreEnd
		}
		
		return $this->_visitor->visitTriple($information);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitor::visitObject()
	 * @return null|boolean
	 */
	public function visitObject(InformationObjectInterface $information) : ?bool
	{
		if($information->isCacheable())
		{
			$pkeys = $information->getPrimaryKey();
			\ksort($pkeys);
			$keys = [];
			
			foreach($pkeys as $key => $value)
			{
				$keys[] = $key.'='.$value;
			}
			$key = \sha1(\implode('|', $keys));
			
			try
			{
				$cacheItem = $this->_cache->getItem($key);
				$etag = $information->getEtag();
				if($cacheItem->isHit() && $cacheItem->get() === $etag)
				{
					return true;
				}
				$cacheItem->set($etag);
				$this->_cache->save($cacheItem);
			}
			// @codeCoverageIgnoreStart
			catch(\Psr\Cache\InvalidArgumentException $exc)
			{
				// ignore, should not happen (keys are hashed)
			}
			// @codeCoverageIgnoreEnd
		}
		
		return $this->_visitor->visitObject($information);
	}
	
}
