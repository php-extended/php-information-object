<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

/**
 * InformationObject class file.
 * 
 * This class is a simple implementation of the InformationObjectInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class InformationObject extends AbstractInformation implements InformationObjectInterface
{
	
	/**
	 * The keys, key as field name, value as value.
	 * 
	 * @var array<string, string>
	 */
	protected array $_keys = [];
	
	/**
	 * The datas, key as field name, value as value.
	 * 
	 * @var array<string, string>
	 */
	protected array $_datas = [];
	
	/**
	 * The relations.
	 * 
	 * @var array<string, string>
	 */
	protected array $_relations = [];
	
	/**
	 * Whether this information is cacheable.
	 * 
	 * @var boolean
	 */
	protected bool $_cacheable;
	
	/**
	 * Builds a new InformationObject with the given key informations.
	 * 
	 * @param string $infoId
	 * @param string $supportClass
	 * @param array<string, ?string> $primaryKey
	 * @param ?boolean $cacheable, null if automatic
	 */
	public function __construct(string $infoId, string $supportClass, array $primaryKey = [], ?bool $cacheable = null)
	{
		parent::__construct($infoId, $supportClass);
		$this->addAllKey($primaryKey);
		if(null === $cacheable)
		{
			$cacheable = true;
			
			foreach(\array_keys($primaryKey) as $fieldName)
			{
				// date prefix in primary keys implies historisation
				foreach(['datetime_', 'time_', 'date_'] as $prefix)
				{
					if(\mb_strpos($fieldName, $prefix) === 0)
					{
						$cacheable = false;
						break 2;
					}
				}
			}
		}
		$this->_cacheable = $cacheable;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\AbstractInformation::__toString()
	 */
	public function __toString() : string
	{
		$ids = [];
		
		foreach($this->_keys as $fieldName => $value)
		{
			$ids[] = $fieldName.'::'.$value;
		}
		
		$fieldNames = \array_keys($this->_datas);
		
		$relations = [];
		
		foreach($this->_relations as $relName => $value)
		{
			$relations[] = $relName.'::'.$value;
		}
		
		return 'OBJECT ['.$this->_infoId.'] { '.$this->_supportClass.'@['.\implode(',', $ids).'] => '.\implode(',', $fieldNames).' & '.\implode(',', $relations).' }';
	}
	
	/**
	 * Gets whether the field name should not be hashed.
	 * 
	 * @param string $fieldName
	 * @return boolean
	 */
	public function isRawField(string $fieldName) : bool
	{
		foreach([
			// boolean fields
			'boolean_', 'bool_', 'is_', 'has_',
			// integer fields
			'integer_', 'int_', 'nb_', 'qty_',
			// float fields
			'float_', 'double_',
			// date fields
			'datetime_', 'time_', 'date_',
		] as $prefix)
		{
			if(\str_starts_with($fieldName, $prefix))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Adds all the given keys.
	 * 
	 * @param array<string, null|boolean|integer|float|string> $primaryKey
	 * @return self
	 */
	public function addAllKey(array $primaryKey) : InformationObject
	{
		foreach($primaryKey as $fieldName => $value)
		{
			if(null === $value)
			{
				continue;
			}
			
			$this->addKey((string) $fieldName, (string) $value);
		}
		
		return $this;
	}
	
	/**
	 * Adds the given key.
	 * 
	 * @param string $fieldName
	 * @param string $value
	 * @return self
	 */
	public function addKey(string $fieldName, ?string $value) : InformationObject
	{
		if('' !== $fieldName && null !== $value && '' !== $value)
		{
			$this->_keys[$fieldName] = $value;
		}
		
		return $this;
	}
	
	/**
	 * Adds all the given data.
	 * 
	 * @param array<string, null|boolean|integer|float|string> $data
	 * @return self
	 */
	public function addAllData(array $data) : InformationObject
	{
		foreach($data as $fieldName => $value)
		{
			if(null === $value)
			{
				continue;
			}
			
			$this->addData((string) $fieldName, (string) $value);
		}
		
		return $this;
	}
	
	/**
	 * Adds the given data.
	 * 
	 * @param string $fieldName
	 * @param string $value
	 * @return self
	 */
	public function addData(string $fieldName, ?string $value) : InformationObject
	{
		if('' !== $fieldName && null !== $value && '' !== $value)
		{
			$this->_datas[$fieldName] = $value;
		}
		
		return $this;
	}
	
	/**
	 * Adds all the given relations.
	 * 
	 * @param array<string, null|boolean|integer|float|string> $relations
	 * @return self
	 */
	public function addAllRelations(array $relations) : InformationObject
	{
		foreach($relations as $relName => $value)
		{
			if(null === $value)
			{
				continue;
			}
			
			$this->addRelation((string) $relName, (string) $value);
		}
		
		return $this;
	}
	
	/**
	 * Adds the given relation.
	 * 
	 * @param string $relName
	 * @param string $value
	 * @return self
	 */
	public function addRelation(string $relName, ?string $value) : InformationObject
	{
		if('' === $relName || null === $value || '' === $value)
		{
			return $this;
		}
		
		$this->_relations[$relName] = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getPrimaryKey()
	 */
	public function getPrimaryKey() : array
	{
		return $this->_keys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getPrimaryKeyMd5()
	 */
	public function getPrimaryKeyMd5() : array
	{
		$nkeys = [];
		
		foreach($this->_keys as $fieldName => $value)
		{
			$nkeys[$fieldName] = \md5($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getSmartPrimaryKeyMd5()
	 */
	public function getSmartPrimaryKeyMd5() : array
	{
		$nkeys = [];
		
		foreach($this->_keys as $fieldName => $value)
		{
			$nkeys[$fieldName] = $this->isRawField($fieldName) ? $value : \md5($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getPrimaryKeySha1()
	 */
	public function getPrimaryKeySha1() : array
	{
		$nkeys = [];
		
		foreach($this->_keys as $fieldName => $value)
		{
			$nkeys[$fieldName] = \sha1($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getSmartPrimaryKeySha1()
	 */
	public function getSmartPrimaryKeySha1() : array
	{
		$nkeys = [];
		
		foreach($this->_keys as $fieldName => $value)
		{
			$nkeys[$fieldName] = $this->isRawField($fieldName) ? $value : \sha1($value);
		}
		
		return $nkeys;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationDatas()
	 */
	public function getInformationDatas() : array
	{
		return $this->_datas;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationData()
	 */
	public function getInformationData(?string $name) : ?string
	{
		return $this->_datas[(string) $name] ?? null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::removeInformationData()
	 */
	public function removeInformationData(?string $name) : bool
	{
		$exists = isset($this->_datas[(string) $name]);
		
		unset($this->_datas[(string) $name]);
		
		return $exists;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationRelations()
	 */
	public function getInformationRelations() : array
	{
		return $this->_relations;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::getInformationRelation()
	 */
	public function getInformationRelation(?string $name) : ?string
	{
		return $this->_relations[(string) $name] ?? null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationObjectInterface::removeInformationRelation()
	 */
	public function removeInformationRelation(?string $name) : bool
	{
		$exists = isset($this->_relations[(string) $name]);
		
		unset($this->_relations[(string) $name]);
		
		return $exists;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::isCacheable()
	 */
	public function isCacheable() : bool
	{
		return $this->_cacheable;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getEtag()
	 */
	public function getEtag() : string
	{
		$key = $this->_keys;
		\ksort($key);
		$spk = (string) \json_encode($key);
		$data = $this->_datas;
		\ksort($data);
		$sdata = (string) \json_encode($data);
		$rel = $this->_relations;
		\ksort($rel);
		$srel = (string) \json_encode($rel);
		
		return \sha1($spk.'|'.$sdata.'|'.$srel);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::beVisitedBy()
	 */
	public function beVisitedBy(InformationVisitorInterface $visitor)
	{
		return $visitor->visitObject($this);
	}
	
}
