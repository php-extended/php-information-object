<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use Exception;
use Iterator;
use Psr\Log\LoggerInterface;

/**
 * LoggerInformationVisitor class file.
 * 
 * This class is a visitor that forwards to another visitor the informations
 * that passed through it and logs a message for each of them. It acts as a
 * decorator for such visitor.
 * 
 * @author Anastaszor
 * @implements \PhpExtended\Information\InformationVisitorInterface<boolean>
 * @extends \PhpExtended\Information\InformationVisitor<boolean>
 */
class LoggerInformationVisitor extends InformationVisitor implements InformationVisitorInterface
{
	
	/**
	 * The logger that this object will use.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The visitor to which all the informations will be forwarded.
	 * 
	 * @var InformationVisitorInterface<boolean>
	 */
	protected InformationVisitorInterface $_visitor;
	
	/**
	 * Builds a new LoggerInformationVisitor with the given logger and inner
	 * visitor.
	 * 
	 * @param LoggerInterface $logger
	 * @param InformationVisitorInterface<boolean> $visitor
	 */
	public function __construct(LoggerInterface $logger, InformationVisitorInterface $visitor)
	{
		$this->_logger = $logger;
		$this->_visitor = $visitor;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitIterator()
	 * @param Iterator<InformationInterface> $informationIterator
	 * @return null|boolean
	 */
	public function visitIterator(Iterator $informationIterator) : ?bool
	{
		$lres = true;
		
		foreach($informationIterator as $information)
		{
			$lres = $this->visitInformation($information) && $lres;
		}
		
		return $lres;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitTriple()
	 * @return null|boolean
	 */
	public function visitTriple(InformationTripleInterface $information) : ?bool
	{
		try
		{
			$return = (bool) $information->beVisitedBy($this->_visitor);
		}
		catch(Exception $e)
		{
			$this->_logger->error('[{verb}][{id}] Information Triple {subject} # {predicate} # {object} : {msg}', [
				'verb' => ' KO ',
				'id' => $information->getId(),
				'subject' => $information->getSubject(),
				'predicate' => $information->getPredicate(),
				'object' => $information->getObject(),
				'msg' => $e,
			]);
			
			throw $e;
		}
		
		$this->_logger->info('[{verb}][{id}] Information Triple {subject} # {predicate} # {object} : {msg}', [
			'verb' => $return ? ' OK ' : 'SKIP',
			'id' => $information->getId(),
			'subject' => $information->getSubject(),
			'predicate' => $information->getPredicate(),
			'object' => $information->getObject(),
		]);
		
		return $return;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationVisitorInterface::visitObject()
	 * @return null|boolean
	 */
	public function visitObject(InformationObjectInterface $information) : ?bool
	{
		try
		{
			$return = (bool) $information->beVisitedBy($this->_visitor);
		}
		catch(Exception $e)
		{
			$ids = [];
			
			foreach($information->getPrimaryKey() as $fieldName => $value)
			{
				$ids[] = $fieldName.'::'.$value;
			}
			
			$fields = \array_merge(
				\array_keys($information->getInformationDatas()),
				\array_keys($information->getInformationRelations()),
			);
			
			$this->_logger->error('[{verb}][{id}] Information Object {class} # {objid} :: {fields} : {msg}', [
				'verb' => ' KO ',
				'id' => $information->getId(),
				'class' => $information->getSupportClass(),
				'objid' => '['.\implode(' | ', $ids).']',
				'fields' => \implode(', ', $fields),
				'msg' => $e,
			]);
			
			throw $e;
		}
		
		$ids = [];
		
		foreach($information->getPrimaryKey() as $fieldName => $value)
		{
			$ids[] = $fieldName.'::'.$value;
		}
		
		$fields = \array_merge(
			\array_keys($information->getInformationDatas()),
			\array_keys($information->getInformationRelations()),
		);
		
		$this->_logger->info('[{verb}][{id}] Information Object {class} # {objid} :: {fields}', [
			'verb' => $return ? ' OK ' : 'SKIP',
			'id' => $information->getId(),
			'class' => $information->getSupportClass(),
			'objid' => '['.\implode(' | ', $ids).']',
			'fields' => \implode(', ', $fields),
		]);
		
		return $return;
	}
	
}
