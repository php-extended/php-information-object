<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use DateTimeImmutable;
use DateTimeInterface;

/**
 * AbstractInformation class file.
 * 
 * This class is the base class for all informations.
 * 
 * @author Anastaszor
 */
abstract class AbstractInformation implements InformationInterface
{
	
	/**
	 * The id of this information.
	 * 
	 * @var string
	 */
	protected string $_infoId;
	
	/**
	 * The date of creation of this information.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_creationDate;
	
	/**
	 * The state of this information.
	 * 
	 * @var InformationStateInformation
	 */
	protected InformationStateInformation $_state;
	
	/**
	 * The class of the object that holds the information.
	 * 
	 * @var string
	 */
	protected string $_supportClass;
	
	/**
	 * Builds a new Information with its class.
	 * 
	 * @param string $infoId
	 * @param string $supportClass
	 */
	public function __construct(string $infoId, string $supportClass)
	{
		$this->_infoId = $infoId;
		$this->_supportClass = $supportClass;
		$this->_creationDate = new DateTimeImmutable();
		$this->_state = new InformationState(true, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_infoId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getCreationDate()
	 */
	public function getCreationDate() : DateTimeInterface
	{
		return $this->_creationDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getState()
	 */
	public function getState() : InformationStateInformation
	{
		return $this->_state;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getSupportClass()
	 */
	public function getSupportClass() : string
	{
		return $this->_supportClass;
	}
	
}
