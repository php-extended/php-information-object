<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

/**
 * InformationState class file.
 * 
 * This class is a simple implementation of the InformationStateInterface.
 * 
 * @author Anastaszor
 */
class InformationState implements InformationStateInformation
{
	
	/**
	 * Whether this state is pending.
	 * 
	 * @var boolean
	 */
	protected bool $_pending = true;
	
	/**
	 * Whether this state is rejected.
	 * 
	 * @var boolean
	 */
	protected bool $_rejected = false;
	
	/**
	 * Builds a new InformationState with the given status.
	 * 
	 * @param boolean $pending
	 * @param boolean $rejected
	 */
	public function __construct(bool $pending, bool $rejected)
	{
		$this->_pending = $pending;
		$this->_rejected = $rejected;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_pending ? 'PENDING' : ($this->_rejected ? 'REJECTED' : 'FULFILLED');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationStateInformation::isPending()
	 */
	public function isPending() : bool
	{
		return $this->_pending;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationStateInformation::isRejected()
	 */
	public function isRejected() : bool
	{
		return $this->_rejected;
	}
	
}
