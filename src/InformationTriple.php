<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

/**
 * InformationTriple class file.
 * 
 * This class is a simple implementation of the InformationTripleInterface.
 * 
 * @author Anastaszor
 */
class InformationTriple extends AbstractInformation implements InformationTripleInterface
{
	
	/**
	 * The subject of the information.
	 * 
	 * @var string
	 */
	protected string $_subject;
	
	/**
	 * The object of the relation.
	 * 
	 * @var ?string
	 */
	protected ?string $_object;
	
	/**
	 * Builds a new Information Triple with subject, predicate and object 
	 * values.
	 * 
	 * @param string $infoId
	 * @param string $subject
	 * @param string $predicate
	 * @param ?string $object
	 */
	public function __construct(string $infoId, string $subject, string $predicate, ?string $object)
	{
		parent::__construct($infoId, $predicate);
		$this->_subject = $subject;
		$this->_object = $object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\AbstractInformation::__toString()
	 */
	public function __toString() : string
	{
		return 'TRIPLE ['.$this->_infoId.'] { '.$this->_subject.' => '.$this->_supportClass.' }';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationTripleInterface::getSubject()
	 */
	public function getSubject() : string
	{
		return $this->_subject;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationTripleInterface::getPredicate()
	 */
	public function getPredicate() : string
	{
		return $this->getSupportClass();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationTripleInterface::getObject()
	 */
	public function getObject() : ?string
	{
		return $this->_object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::isCacheable()
	 */
	public function isCacheable() : bool
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::getEtag()
	 */
	public function getEtag() : string
	{
		return \sha1($this->_subject.'|'.((string) $this->_object));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Information\InformationInterface::beVisitedBy()
	 */
	public function beVisitedBy(InformationVisitorInterface $visitor)
	{
		return $visitor->visitTriple($this);
	}
	
}
