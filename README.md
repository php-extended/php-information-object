# php-extended/php-information-object
A library to transfer informations from a system to another

![coverage](https://gitlab.com/php-extended/php-information-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-information-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-information-object ^18`


## Basic Usage

This library is made to convoy single informations as long with some of its
location metadata. Such locations include for Data informations, the class
of the object it is attached, its identifier and the field in which the
information should be placed. For Relation informations, they are the class and
the identifier of the source and the target objects.


## License

MIT (See [license file](LICENSE)).
