<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationObject;
use PhpExtended\Information\InformationVisitor;
use PHPUnit\Framework\TestCase;

/**
 * InformationObjectTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\InformationObject
 *
 * @internal
 *
 * @small
 */
class InformationObjectTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InformationObject
	 */
	protected InformationObject $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('OBJECT [infoId] { supportClass@[key::value] => field & targetName::targetId }', $this->_object->__toString());
	}
	
	public function testIsRawField() : void
	{
		$this->assertTrue($this->_object->isRawField('is_toto'));
	}
	
	public function testAddAllKey() : void
	{
		$this->_object->addAllKey(['nkey' => 'nval', 'nnkey' => null]);
		
		$this->assertCount(2, $this->_object->getPrimaryKey());
	}
	
	public function testAddEmptyKey() : void
	{
		$this->_object->addKey('toto', null);
		
		$this->assertCount(1, $this->_object->getPrimaryKey());
	}
	
	public function testAddAllData() : void
	{
		$this->_object->addAllData(['nkey' => 'nval', 'nnkey' => null]);
		
		$this->assertCount(2, $this->_object->getInformationDatas());
	}
	
	public function testAddEmptyData() : void
	{
		$this->_object->addData('toto', null);
		
		$this->assertCount(1, $this->_object->getInformationDatas());
	}
	
	public function testAddAllRelations() : void
	{
		$this->_object->addAllRelations(['nkey' => 'nval', 'nnkey' => null]);
		
		$this->assertCount(2, $this->_object->getInformationRelations());
	}
	
	public function testAddEmptyRelation() : void
	{
		$this->_object->addRelation('toto', null);
		
		$this->assertCount(1, $this->_object->getInformationRelations());
	}
	
	public function testGetPrimaryKeys() : void
	{
		$this->assertEquals(['key' => 'value'], $this->_object->getPrimaryKey());
	}
	
	public function testGetPrimaryKeyMd5() : void
	{
		$this->assertEquals(['key' => \md5('value')], $this->_object->getPrimaryKeyMd5());
	}
	
	public function testGetSmartPrimaryKeyMd5() : void
	{
		$this->assertEquals(['key' => \md5('value')], $this->_object->getSmartPrimaryKeyMd5());
	}
	
	public function testGetPrimaryKeySha1() : void
	{
		$this->assertEquals(['key' => \sha1('value')], $this->_object->getPrimaryKeySha1());
	}
	
	public function testGetSmartPrimaryKeySha1() : void
	{
		$this->assertEquals(['key' => \sha1('value')], $this->_object->getSmartPrimaryKeySha1());
	}
	
	public function testGetInformationDatas() : void
	{
		$this->assertEquals(['field' => 'data'], $this->_object->getInformationDatas());
	}
	
	public function testGetInformationData() : void
	{
		$this->assertEquals('data', $this->_object->getInformationData('field'));
	}
	
	public function testRemoveInformationData() : void
	{
		$this->assertTrue($this->_object->removeInformationData('field'));
	}
	
	public function testGetInformationRelations() : void
	{
		$this->assertEquals(['targetName' => 'targetId'], $this->_object->getInformationRelations());
	}
	
	public function testGetInformationRelation() : void
	{
		$this->assertEquals('targetId', $this->_object->getInformationRelation('targetName'));
	}
	
	public function testRemoveInformationRelation() : void
	{
		$this->assertTrue($this->_object->removeInformationRelation('targetName'));
	}
	
	public function testGetEtag() : void
	{
		$this->assertEquals('b0530674d8b48113dbb80b0121a6e3b0e5bda0f1', $this->_object->getEtag());
	}
	
	public function testIsCacheable() : void
	{
		$this->assertTrue($this->_object->isCacheable());
	}
	
	public function testIsNotCachable() : void
	{
		$info = new InformationObject('id', 'class', ['date_thing' => 'date_value']);
		$this->assertFalse($info->isCacheable());
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new InformationVisitor()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InformationObject('infoId', 'supportClass');
		$this->_object->addKey('key', 'value');
		$this->_object->addData('field', 'data');
		$this->_object->addRelation('targetName', 'targetId');
	}
	
}
