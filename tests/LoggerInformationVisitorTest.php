<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationVisitorInterface;
use PhpExtended\Information\LoggerInformationVisitor;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * LoggerInformationVisitorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\LoggerInformationVisitor
 *
 * @internal
 *
 * @small
 */
class LoggerInformationVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LoggerInformationVisitor
	 */
	protected LoggerInformationVisitor $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LoggerInformationVisitor(
			$this->getMockForAbstractClass(LoggerInterface::class),
			$this->getMockForAbstractClass(InformationVisitorInterface::class),
		);
	}
	
}
