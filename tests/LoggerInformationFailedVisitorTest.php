<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationInterface;
use PhpExtended\Information\InformationObject;
use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTriple;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitorInterface;
use PhpExtended\Information\LoggerInformationVisitor;
use PHPUnit\Framework\TestCase;
use Psr\Log\AbstractLogger;

/**
 * LoggerInformationFailedVisitorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\LoggerInformationVisitor
 *
 * @internal
 *
 * @small
 */
class LoggerInformationFailedVisitorTest extends TestCase
{
	
	/**
	 * The logger to assist.
	 * 
	 * @var AbstractLogger
	 */
	protected AbstractLogger $_logger;
	
	/**
	 * The object to test.
	 * 
	 * @var LoggerInformationVisitor
	 */
	protected LoggerInformationVisitor $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitTriple() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->assertNull($this->_logger->logd);
		
		try
		{
			$this->assertTrue($this->_object->visitTriple(new InformationTriple('c', 'd', 'e', 'f')));
		}
		catch(Throwable $e)
		{
			$this->assertIsString($this->_logger->logd);
			
			throw $e;
		}
	}
	
	public function testVisitObject() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->assertNull($this->_logger->logd);
		
		$information = new InformationObject('c', 'd');
		$information->addKey('pk', 'pkval');
		$information->addData('f', 'g');
		$information->addRelation('h', 'i');
		
		try
		{
			$this->assertTrue($this->_object->visitObject($information));
		}
		catch(Throwable $e)
		{
			$this->assertIsString($this->_logger->logd);
			
			throw $e;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_logger = new class() extends AbstractLogger
		{
			
			public $logd;
			
			public function log($level, $message, array $context = []) : void
			{
				$nctx = [];
				
				foreach($context as $k => $value)
				{
					$nctx['{'.$k.'}'] = $value;
				}
				
				$this->logd = '['.$level.'] '.\strtr($message, $nctx);
			}
		};
		
		$visitor = new class() implements InformationVisitorInterface
		{
			
			public function __toString() : string
			{
				return __CLASS__;
			}
			
			public function visitAll(array $informationIterator)
			{
				return true;
			}
			
			public function visitIterator(Iterator $informationIterator)
			{
				return true;
			}
			
			public function visitInformation(InformationInterface $information)
			{
				return true;
			}
			
			public function visitTriple(InformationTripleInterface $information) : void
			{
				throw new InvalidArgumentException();
			}
			
			public function visitObject(InformationObjectInterface $information) : void
			{
				throw new InvalidArgumentException();
			}
		};
		
		$this->_object = new LoggerInformationVisitor($this->_logger, $visitor);
	}
	
}
