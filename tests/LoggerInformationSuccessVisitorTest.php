<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationInterface;
use PhpExtended\Information\InformationObject;
use PhpExtended\Information\InformationObjectInterface;
use PhpExtended\Information\InformationTriple;
use PhpExtended\Information\InformationTripleInterface;
use PhpExtended\Information\InformationVisitorInterface;
use PhpExtended\Information\LoggerInformationVisitor;
use PHPUnit\Framework\TestCase;
use Psr\Log\AbstractLogger;

/**
 * LoggerInformationSuccessVisitorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\LoggerInformationVisitor
 *
 * @internal
 *
 * @small
 */
class LoggerInformationSuccessVisitorTest extends TestCase
{
	
	/**
	 * The logger to assist.
	 * 
	 * @var AbstractLogger
	 */
	protected AbstractLogger $_logger;
	
	/**
	 * The object to test.
	 * 
	 * @var LoggerInformationVisitor
	 */
	protected LoggerInformationVisitor $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitIterator() : void
	{
		$this->assertNull($this->_logger->logd);
		
		$this->assertTrue($this->_object->visitIterator(new ArrayIterator([new InformationTriple('c', 'd', 'e', 'f')])));
		
		$this->assertIsString($this->_logger->logd);
	}
	
	public function testVisitTriple() : void
	{
		$this->assertNull($this->_logger->logd);
		
		$this->assertTrue($this->_object->visitTriple(new InformationTriple('c', 'd', 'e', 'f')));
		
		$this->assertIsString($this->_logger->logd);
	}
	
	public function testVisitObject() : void
	{
		$this->assertNull($this->_logger->logd);
		
		$information = new InformationObject('c', 'd');
		$information->addKey('z', 'y');
		$information->addData('e', 'f');
		$information->addRelation('g', 'h');
		
		$this->assertTrue($this->_object->visitObject($information));
		
		$this->assertIsString($this->_logger->logd);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_logger = new class() extends AbstractLogger
		{
			
			public $logd;
			
			public function log($level, $message, array $context = []) : void
			{
				$nctx = [];
				
				foreach($context as $k => $value)
				{
					$nctx['{'.$k.'}'] = $value;
				}
				
				$this->logd = '['.$level.'] '.\strtr($message, $nctx);
			}
		};
		
		$visitor = new class() implements InformationVisitorInterface
		{
			
			public function __toString() : string
			{
				return __CLASS__;
			}
			
			public function visitAll(array $informationIterator)
			{
				return true;
			}
			
			public function visitIterator(Iterator $informationIterator)
			{
				return true;
			}
			
			public function visitInformation(InformationInterface $information)
			{
				return true;
			}
			
			public function visitTriple(InformationTripleInterface $information)
			{
				return true;
			}
			
			public function visitObject(InformationObjectInterface $information)
			{
				return true;
			}
		};
		
		$this->_object = new LoggerInformationVisitor($this->_logger, $visitor);
	}
	
}
