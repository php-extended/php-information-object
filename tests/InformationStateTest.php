<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationState;
use PHPUnit\Framework\TestCase;

/**
 * InformationStateTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\InformationState
 *
 * @internal
 *
 * @small
 */
class InformationStateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InformationState
	 */
	protected InformationState $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PENDING', $this->_object->__toString());
	}
	
	public function testIsPending() : void
	{
		$this->assertTrue($this->_object->isPending());
	}
	
	public function testIsRejected() : void
	{
		$this->assertFalse($this->_object->isRejected());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InformationState(true, false);
	}
	
}
