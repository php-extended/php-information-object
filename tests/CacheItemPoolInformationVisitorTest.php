<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\CacheItemPoolInformationVisitor;
use PhpExtended\Information\InformationObject;
use PhpExtended\Information\InformationTriple;
use PhpExtended\Information\InformationVisitorInterface;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

if(!\class_exists('CacheableTriple'))
{
	class CacheableTriple extends InformationTriple
	{
		public function isCacheable() : bool
		{
			return true;
		}
	}
}

/**
 * CacheItemPoolInformationVisitorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\CacheItemPoolInformationVisitor
 *
 * @internal
 *
 * @small
 */
class CacheItemPoolInformationVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CacheItemPoolInformationVisitor
	 */
	protected CacheItemPoolInformationVisitor $_object;
	
	protected ?string $_hashval;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitTriple() : void
	{
		$this->_hashval = '4b792e73090292dd0e911d71b325d1bd3c5373cc';
		$triple = new CacheableTriple('info', 'subject', 'predicate', 'object');
		$this->assertEquals($this->_hashval, $triple->getEtag());
		// first it works
		$this->assertTrue($this->_object->visitTriple($triple));
		// second it doesnot
		$this->assertNull($this->_object->visitTriple($triple));
	}
	
	public function testVisitObject() : void
	{
		$this->_hashval = '08a491c96e2c73d5f95f8949f249fba13eb9359a';
		$object = new InformationObject('info', 'class');
		$object->addKey('keyfield', 'keyval');
		$object->addData('dataName', 'dataValue');
		$this->assertEquals($this->_hashval, $object->getEtag());
		// first it works
		$this->assertTrue($this->_object->visitObject($object));
		// second it doesnot
		$this->assertNull($this->_object->visitObject($object));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$successItemMock = $this->createMock(CacheItemInterface::class);
		$successItemMock->expects($this->any())->method('isHit')->willReturn(true);
		$test = $this;
		$successItemMock->expects($this->any())->method('get')->willReturnCallback(function() use ($test)
		{ return $test->_hashval; });
		$failedItemMock = $this->createMock(CacheItemInterface::class);
		$failedItemMock->expects($this->any())->method('isHit')->willReturn(false);
		$cacheMock = $this->createMock(CacheItemPoolInterface::class);
		$cacheMock->expects($this->any())->method('getItem')->willReturn($successItemMock, $failedItemMock);
		$visitorMock = $this->createMock(InformationVisitorInterface::class);
		$visitorMock->expects($this->any())->method('visitTriple')->willReturn(null);
		$visitorMock->expects($this->any())->method('visitObject')->willReturn(null);
		$this->_object = new CacheItemPoolInformationVisitor($cacheMock, $visitorMock);
	}
	
}
