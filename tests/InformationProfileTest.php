<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationObject;
use PhpExtended\Information\InformationProfile;
use PHPUnit\Framework\TestCase;

/**
 * InformationProfileTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\InformationProfile
 *
 * @internal
 *
 * @small
 */
class InformationProfileTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InformationProfile
	 */
	protected InformationProfile $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@category', $this->_object->__toString());
	}
	
	public function testAddInformation() : void
	{
		$this->_object->handle(new InformationObject('', ''));
		$this->assertEquals(1, $this->_object->getNbInformations());
	}
	
	public function testGetInformationCategory() : void
	{
		$this->assertEquals('category', $this->_object->getInformationCategory());
	}
	
	public function testGetNbInformations() : void
	{
		$this->assertEquals(0, $this->_object->getNbInformations());
	}
	
	public function testGetTotalTime() : void
	{
		$this->assertEquals(new SummableDateInterval('PT0S'), $this->_object->getTotalTime());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InformationProfile('category');
	}
	
}
