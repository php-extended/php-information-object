<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationObject;
use PhpExtended\Information\InformationTriple;
use PhpExtended\Information\InformationVisitor;
use PHPUnit\Framework\TestCase;

/**
 * InformationVisitorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\InformationVisitor
 *
 * @internal
 *
 * @small
 */
class InformationVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InformationVisitor
	 */
	protected InformationVisitor $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitAll() : void
	{
		$this->assertNull($this->_object->visitAll([new InformationTriple('c', 'd', 'e', 'f')]));
	}
	
	public function testVisitInformationTriple() : void
	{
		$this->assertNull($this->_object->visitTriple(new InformationTriple('c', 'd', 'e', 'f')));
	}
	
	public function testVisitInformationObject() : void
	{
		$this->assertNull($this->_object->visitObject(new InformationObject('c', 'd')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InformationVisitor();
	}
	
}
