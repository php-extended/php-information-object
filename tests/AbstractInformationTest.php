<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\AbstractInformation;
use PhpExtended\Information\InformationState;
use PHPUnit\Framework\TestCase;

/**
 * AbstractInformationTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\AbstractInformation
 *
 * @internal
 *
 * @small
 */
class AbstractInformationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AbstractInformation
	 */
	protected AbstractInformation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('infoId', $this->_object->getId());
	}
	
	public function testGetCreationDate() : void
	{
		$this->assertInstanceOf(DateTimeImmutable::class, $this->_object->getCreationDate());
	}
	
	public function testGetState() : void
	{
		$this->assertEquals(new InformationState(true, false), $this->_object->getState());
	}
	
	public function testGetSupportClass() : void
	{
		$this->assertEquals('supportClass', $this->_object->getSupportClass());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(AbstractInformation::class, [
			'infoId', 'supportClass',
		]);
	}
	
}
