<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationObject;
use PhpExtended\Information\InformationProfilerVisitor;
use PhpExtended\Information\InformationTriple;
use PHPUnit\Framework\TestCase;

/**
 * InformationProfilerVisitorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\InformationProfilerVisitor
 *
 * @internal
 *
 * @small
 */
class InformationProfilerVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InformationProfilerVisitor
	 */
	protected InformationProfilerVisitor $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitIterator() : void
	{
		$this->assertTrue($this->_object->visitIterator(new ArrayIterator([new InformationTriple('c', 'd', 'e', 'f')])));
	}
	
	public function testVisitTriple() : void
	{
		$this->assertTrue($this->_object->visitTriple(new InformationTriple('c', 'd', 'e', 'f')));
	}
	
	public function testVisitObject() : void
	{
		$this->assertTrue($this->_object->visitObject(new InformationObject('c', 'd')));
	}
	
	public function testGetTripleProfiles() : void
	{
		$this->assertEquals([], $this->_object->getTripleProfiles());
	}
	
	public function testGetObjectProfiles() : void
	{
		$this->assertEquals([], $this->_object->getObjectProfiles());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InformationProfilerVisitor();
	}
	
}
