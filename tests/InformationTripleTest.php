<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Information\InformationTriple;
use PhpExtended\Information\InformationVisitor;
use PHPUnit\Framework\TestCase;

/**
 * InformationTripleTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Information\InformationTriple
 *
 * @internal
 *
 * @small
 */
class InformationTripleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InformationTriple
	 */
	protected InformationTriple $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('TRIPLE [infoId] { subject => predicate }', $this->_object->__toString());
	}
	
	public function testGetSubject() : void
	{
		$this->assertEquals('subject', $this->_object->getSubject());
	}
	
	public function testGetPredicate() : void
	{
		$this->assertEquals('predicate', $this->_object->getPredicate());
	}
	
	public function testGetObject() : void
	{
		$this->assertEquals('object', $this->_object->getObject());
	}
	
	public function testIsCacheable() : void
	{
		$this->assertFalse($this->_object->isCacheable());
	}
	
	public function testGetEtag() : void
	{
		$this->assertEquals('4b792e73090292dd0e911d71b325d1bd3c5373cc', $this->_object->getEtag());
	}
	
	public function testBeVisitedBy() : void
	{
		$this->assertNull($this->_object->beVisitedBy(new InformationVisitor()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InformationTriple('infoId', 'subject', 'predicate', 'object');
	}
	
}
